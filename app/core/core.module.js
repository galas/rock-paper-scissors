'use strict';

import MoveGeneratorService from './move-generator/move-generator.service';

export default angular.module('App.Core', [MoveGeneratorService]).name;
