describe('App.Core.MoveGeneratorService', () => {
  let MoveGeneratorService;

  beforeEach(() => {
    module('App.Core.MoveGenerator');
  });

  beforeEach(inject((_MoveGeneratorService_) => {
    MoveGeneratorService = _MoveGeneratorService_;
  }));

  it('should initialise', () => {
    expect(MoveGeneratorService).toBeDefined();
  });

  it('should return a random number under the supplied limit', () => {
    expect(MoveGeneratorService.getMove(3)).toBeLessThan(3);
  });

  it('shoud return a positive integer', () => {
    const number = MoveGeneratorService.getMove(5);
    expect(number).toBe(Math.floor(Math.abs(number)));
  })
});
