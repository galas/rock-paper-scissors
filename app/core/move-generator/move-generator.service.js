'use strict';

class MoveGeneratorService {
  getMove(limit) {
    return Math.floor(Math.random() * limit);
  }
}

export default angular.module('App.Core.MoveGenerator', [])
  .service('MoveGeneratorService', MoveGeneratorService)
  .name;
