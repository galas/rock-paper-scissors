describe('App.Components.Game', () => {
  let $rootScope;
  let $compile;
  let scope;
  let element;
  let template = '<game></game>';

  beforeEach(() => {
    module('App.Components.Game');
    module('App.Core.MoveGenerator');
    module('my.templates');
  });

  beforeEach(inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  beforeEach(() => {
    let templateScope = $rootScope.$new();
    element = $compile(template)(templateScope);
    templateScope.$digest();
    scope = element.isolateScope();
  });

  it('should initialise', () => {
    expect(scope.component).toBeDefined();
  });

  describe('Game.Controller', () => {
    beforeEach(() => {
      spyOn(scope.component.GameService, 'play').and.callThrough();
    });

    it('should have moves', () => {
      expect(scope.component.moves).toBeDefined();
    });

    it('should get a result when the user picks a move', () => {
      scope.component.inputMove(1);
      expect(scope.component.GameService.play).toHaveBeenCalled();
    });

    it('should get the result', () => {
      scope.component.inputMove(2);
      expect(scope.component.result).toBeDefined();
    });

    it('should get the computer move', () => {
        scope.component.inputMove(2);
        expect(scope.component.computerMove).toBeDefined();
    });

    it('should start a new game after obtaining results', () => {
      scope.component.inputMove(2);
      scope.component.restart();
      expect(scope.component.result).toBeNull();
    });
  });
});
