'use strict';

export default {
  moves: ['rock', 'paper', 'scissors'],
  results: ['draw', 'lose', 'win']
}
