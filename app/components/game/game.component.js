'use strict';

import template from './game.html';

class GameController {
  constructor(GAME_CONSTANTS, GameService) {
    this.moves = GAME_CONSTANTS.moves;
    this.GameService = GameService;
  }

  inputMove(userMove) {
    this.userMove = userMove;
    const result = this.GameService.play(userMove);

    this.result = result.result;
    this.computerMove = result.computerMove;
  }

  restart() {
    this.userMove = null;
    this.result = null;
    this.computerMove = null;
  }
}

export default {
    controller: GameController,
    controllerAs: 'component',
    templateUrl: template
  };
