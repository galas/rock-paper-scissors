describe('App.Core.GameService', () => {
  let GameService;
  let move = 2;

  beforeEach(() => {
    module('App.Components.Game');
    module('App.Core.MoveGenerator');
  });


  beforeEach(inject((_GameService_) => {
    GameService = _GameService_;
  }));

  beforeEach(() => {
    spyOn(GameService.MoveGeneratorService, 'getMove')
      .and.callFake(() => move);
  })

  it('should initialise', () => {
    expect(GameService).toBeDefined();
  });

  it('should get the AI move', () => {
    GameService.play(0);
    expect(GameService.MoveGeneratorService.getMove).toHaveBeenCalled();
  });

  it('should return a result', () => {
    expect(GameService.play(0)).toEqual(jasmine.any(Object));
  });

  describe('Computer plays Rock', () => {
    beforeEach(() => {
      move = 0;
    });

    it('should return draw when the user plays rock', () => {
      expect(GameService.play(0)).toEqual({ computerMove: 0, result:'draw' });
    });

    it('should return win when the user plays paper', () => {
      expect(GameService.play(1)).toEqual({ computerMove: 0, result:'win' });
    });

    it('should return lose when the user plays scissors', () => {
      expect(GameService.play(2)).toEqual({ computerMove: 0, result: 'lose' });
    });
  });

  describe('Computer plays Paper', () => {
    beforeEach(() => {
      move = 1;
    });

    it('should return draw when the user plays paper', () => {
      expect(GameService.play(1)).toEqual({ computerMove: 1, result:'draw' });
    });

    it('should return win when the user plays scissors', () => {
      expect(GameService.play(2)).toEqual({ computerMove: 1, result: 'win' });
    });

    it('should return lose when the user plays rock', () => {
      expect(GameService.play(0)).toEqual({ computerMove: 1, result: 'lose' });
    });
  });

  describe('Computer plays Scissors', () => {
    beforeEach(() => {
      move = 2;
    });

    it('should return draw when the user plays scissors', () => {
      expect(GameService.play(2)).toEqual({ computerMove: 2, result: 'draw' });
    });

    it('should return lose when the user plays paper', () => {
      expect(GameService.play(1)).toEqual({ computerMove: 2, result: 'lose' });
    });

    it('should return win when the user plays rock', () => {
      expect(GameService.play(0)).toEqual({ computerMove: 2, result:'win' });
    });
  });
});
