'use strict';

import gameComponent from './game.component';
import GameService from './game.service';
import GAME_CONSTANTS from './game.constants';

export default angular.module('App.Components.Game', [])
  .constant('GAME_CONSTANTS', GAME_CONSTANTS)
  .service('GameService', GameService)
  .component('game', gameComponent)
  .name;
