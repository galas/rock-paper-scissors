'use strict';

class GameService {
  constructor(MoveGeneratorService, GAME_CONSTANTS) {
    this.MoveGeneratorService = MoveGeneratorService;
    this.moves = GAME_CONSTANTS.moves;
    this.results = GAME_CONSTANTS.results;
  }

  play(userMove) {
    const computerMove = this._getMove();
    const result = this._calculateResult(userMove, computerMove);

    return {
      computerMove,
      result: this.results[result],
    }
  }

  _calculateResult(player1, player2) {
    return (this.moves.length + player2 - player1) % this.moves.length;
  }

  _getMove() {
    return this.MoveGeneratorService.getMove(this.moves.length);
  }
}

export default GameService;
