'use strict';

import game from './game/game.module';

export default angular.module('App.Components', [game]).name;
