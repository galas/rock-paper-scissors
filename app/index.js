'use strict';

import core from './core/core.module';
import components from './components/components.module';
import css from './main.scss';

angular.module('App', [
  core,
  components
]);

