// Karma configuration
// Generated on Sun Mar 19 2017 01:38:01 GMT+0000 (GMT)

const webpackConfig = require('./webpack.config.js');
const commonsChunkPluginIndex = webpackConfig.plugins.findIndex(plugin => plugin.chunkNames);
webpackConfig.plugins.splice(commonsChunkPluginIndex, 1);


module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'app/vendor.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'app/index.js',
      'app/**/*.spec.js',
      "app/**/*.html"
    ],


    // list of files to exclude
    exclude: [
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      "app/vendor.js": ["webpack"],
      "app/index.js": ["webpack"],
      "app/**/*.html": ["ng-html2js"],
      'app/**/*.spec.js': ["babel"],
    },

    webpack: webpackConfig,

    plugins: [
      'karma-ng-html2js-preprocessor',
      'karma-babel-preprocessor',
      'karma-jasmine',
      'karma-webpack',
      'karma-phantomjs-launcher',
      'karma-spec-reporter',
    ],

    ngHtml2JsPreprocessor: {
      stripPrefix: '(app\/)',
      moduleName: 'my.templates'
    },

    babelPreprocessor: {
      options: {
        presets: ['env'],
        sourceMap: 'inline'
      },
      filename: file => {
        return file.originalPath.replace(/\.js$/, '.es5.js');
      },
      sourceFileName: file => {
        return file.originalPath;
      }
    },

    specReporter: {
    maxLogLines: 5,         // limit number of lines logged per test
    suppressErrorSummary: false,  // do not print error summary
    suppressFailed: false,  // do not print information about failed tests
    suppressPassed: false,  // do not print information about passed tests
    suppressSkipped: true,  // do not print information about skipped tests
    showSpecTiming: false // print the time elapsed for each spec
  },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['spec'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
