'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    main: './app/index.js',
    vendor: './app/vendor.js',

  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor'
    }),
    new HtmlWebpackPlugin({
      template: './app/assets/index.html',
      inject: 'body',
    }),
    new ExtractTextPlugin({
        filename: 'style.css',
        allChunks: true
      })
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets: ['env']
        }
      },
      { test: /\.html$/,
        exclude: /index.html/,
        loader: `ngtemplate-loader?requireAngular&relativeTo=${(path.resolve(__dirname, './app'))}/!html-loader`
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
            loader: "css-loader"
            }, {
            loader: "sass-loader"
            }
          ]
        })
      }
    ]
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.join(__dirname, "app")
  }
};
