# Rock Paper Scissors

This is a basic web implementation of the rock paper scissors game using angularJS and Sass. 

## Instructions:

1. clone the project;
2. Install dependencies with `npm install`;
3. Run `npm run serve` to run an in-memory dev build on `webpack-dev-server`, which will automagically serve the app at `localhost:8080`.
4. Run the tests with `npm run test` (this will run a single-run).

Alternatively, 
`npm run build`
will build the app in the `dist/` directory so it can be interacted with without the need for a server.

## About
The app runs on angular 1.5.11 and is bundled with webpack, as I wanted to see how it played with angular1. 
Sourcemaps are available.

Tests run on Karma with Jasmine, also leveraging webpack. Everything is transpiled with babel.

It was developed on node 6.9.4 LTS on OS X.

## The Implementation 
I am familiar with the Rock Paper Scissors game, but I had not looked at it from a programaming perspective. 

I initially began by writing a simple implementation using an array of moves, their indexes and some basic control flow to decide which move would win over another. 

This worked, but it was not neither a very elegant nor maintainable solution, as as soon as we start assigning a value to a move (in this case, their index) and a value to the result it's easy to observe that there is a cyclical hierarchy between the moves:

`0 loses to 1 loses to 2`;
`2 beats 1 beats 0`;

There are also some variations on the game which have more than 3 moves, and an implementation based on control flow would result in a growing list of conditional statements and/or maps. 

I drew up a table of possible results, and wrote a comprehensive test spec that would cover all the possible combinations. Then I started trying to see if I could observe a pattern from which to draw an effective calculation. 
The wikipedia page provided did hint at using modular arithmetic.  

## What I would have done differently:

1. The UI is rather basic, could do with some polishing;
2. e2e tests would be nice;
3. A test coverage plugin would be nice;
3. I was undecided as what to do with a component with subdependencies on a service, so I ended up treating them as a single component for the purposes of the exercise. 

